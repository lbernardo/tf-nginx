resource "aws_eks_cluster" "cluster" {
  name     = format("app-%s-cluster", var.environment)
  role_arn = aws_iam_role.eks_cluster_role.arn

  version = "1.22"

  vpc_config {
    subnet_ids = [for o in aws_subnet.subnet : o.id]
  }

  tags = {
    Name = format("app-%s-cluster", var.environment)
    Environment = var.environment
    Project = "tf-nginx"
  }

  depends_on = [
    aws_iam_role.eks_cluster_role,
    aws_subnet.subnet,
  ]

}

resource "aws_eks_node_group" "node_group" {
  cluster_name    = aws_eks_cluster.cluster.name
  node_group_name_prefix = format("app-%s-node", var.environment)
  node_role_arn   = aws_iam_role.ec2_iam_role.arn
  subnet_ids      = [for o in aws_subnet.subnet : o.id]

  instance_types = var.instance_types

  scaling_config {
    desired_size = var.nodes_config.initial
    max_size     = var.nodes_config.max
    min_size     = var.nodes_config.min
  }

  update_config {
    max_unavailable = 1
  }

  tags = {
    Name = format("app-%s-cluster", var.environment)
    Environment = var.environment
    Project = "tf-nginx"
  }

  depends_on = [
    aws_eks_cluster.cluster,
    aws_iam_role.ec2_iam_role,
    aws_subnet.subnet,
  ]
}
