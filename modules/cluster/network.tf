resource "aws_vpc" "vpc" {
  cidr_block = var.cidr
  tags       = {
    Name = format("app-%s-vpc", var.environment)
    Environment = var.environment
    Project = "tf-nginx"
  }
}

resource "aws_internet_gateway" "gw_stage" {
  vpc_id = aws_vpc.vpc.id
  tags   = {
    Name = format("app-%s-gw", var.environment)
    Environment = var.environment
    Project = "tf-nginx"
  }
  depends_on = [
    aws_vpc.vpc,
  ]
}

resource "aws_route_table" "rt_stage" {
  vpc_id = aws_vpc.vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw_stage.id
  }

  tags = {
    Name = format("%s-rt", var.environment)
    Environment = var.environment
    Project = "tf-nginx"
  }
  depends_on = [
    aws_vpc.vpc,
    aws_internet_gateway.gw_stage
  ]
}

resource "aws_subnet" "subnet" {
  for_each  = var.subnets
  map_public_ip_on_launch = true
  vpc_id            = aws_vpc.vpc.id
  availability_zone = each.value["avaiable_zone"]
  cidr_block        = each.value["cidr"]
  tags              = {
    Name = format("%s-subnet-%s", var.environment,  each.key)
    Environment = var.environment
    Project = "tf-nginx"
  }
  depends_on = [
    aws_vpc.vpc,
  ]
}

resource "aws_route_table_association" "route_table_association_stage" {
  for_each = var.subnets
  subnet_id      = aws_subnet.subnet[each.key].id
  route_table_id = aws_route_table.rt_stage.id
  depends_on = [
    aws_subnet.subnet,
    aws_route_table.rt_stage,
  ]
}
