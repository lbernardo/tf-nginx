variable "environment" {
  type = string
}

variable "cidr" {
  type = string
}

variable "subnets" {
  type = map(object({ cidr = string, avaiable_zone = string }))
}

variable "instance_types" {
  type = list(string)
}

variable "nodes_config" {
  type = object({initial = number, max = number, min = number})
}