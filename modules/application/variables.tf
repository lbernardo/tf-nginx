variable "environment" {
  type = string
}

variable "cluster_name" {
  type = string
}

variable "cluster_endpoint" {
  type = string
}

variable "cluster_ca_cert" {
}

variable "name" {
  type = string
}

variable "image" {
  type = string
}

variable "replicas" {
  type = number
  default = 1
}

variable "port" {
  type = number
  default = 80
}
