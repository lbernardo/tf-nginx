resource "kubernetes_deployment" "app_deployment" {
  metadata {
    name      = format("%s-%s-deployment", var.environment, var.name)
    namespace = "default"
    labels = {
      app = format("%s-%s", var.environment, var.name)
      environment = var.environment
    }
  }

  spec {
    replicas = var.replicas

    selector {
      match_labels = {
        app = format("%s-%s", var.environment, var.name)
        environment = var.environment
      }
    }
    template {
      metadata {
        labels = {
          app = format("%s-%s", var.environment, var.name)
          environment = var.environment
        }
      }
      spec {
        container {
          image = var.image
          name  = var.name
          port {
            container_port = var.port
          }
        }
      }
    }
  }
}