resource "kubernetes_ingress_v1" "app_ingress" {
  wait_for_load_balancer = true
  metadata {
    name      = format("%s-%s-ingress", var.environment, var.name)
  }
  spec {
    ingress_class_name = "nginx"
    rule {
      http {
        path {
          backend {
            service {
              name = kubernetes_service.app_service.metadata[0].name
              port {
                number = var.port
              }
            }
          }
          path = "/"
          path_type = "Prefix"
        }
      }
    }
  }
}