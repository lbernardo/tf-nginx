resource "kubernetes_service" "app_service" {
  metadata {
    name      = format("%s-service", var.name)
  }
  spec {
    selector = {
      app = format("%s-%s", var.environment, var.name)
    }
    port {
      port        = var.port
      target_port = var.port
    }
    type = "ClusterIP"
  }
}