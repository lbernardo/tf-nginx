module "nginx-controller" {
  source     = "terraform-iaac/nginx-controller/helm"
  namespace  = kubernetes_namespace.ingress_nginx.metadata[0].name
}

