# tf-nginx

- [PT-BR](readme.md)

## Dependencies

- aws-cli [~2.7]
- terraform [v1.2.9]
- kubectl [1.23]

## Initial Settings

It is necessary to configure the terraform backend configuration through the `backend.tf` file.
In the backend.bucket configuration, add the s3 bucket you want to use to save the terraform state file.
This bucket must exist, and the default `profile` must have access permission.
If necessary you can configure the region (`region`) in the backend.region parameter

```terraform
terraform {
   backend "s3" {
     bucket = "example-of-bucket"
     region = "us-east-1"
   }
}
```

You will also need to configure AWS credentials in your terminal

```shell
export AWS_ACCESS_KEY_ID=******
export AWS_SECRET_ACCESS_KEY=*******
```

Or set up a `profile` in the files:

- `backend.tf`
- `modules/addons/providers.tf`
- `modules/application/providers.tf`
- `main.tf`

### Setting environment
There are pre-defined environment settings. They are in `config/environments/<ENVIRONMENT>.tfvars`.
In this file you will find cluster configuration information by environment.

```terraform
environment = "dev"
region = "us-east-1"
cidr = "172.31.0.0/16"
subnets = {
  a = {
    available_zone = "us-east-1a"
    cidr = "172.31.1.0/24"
  }
  b = {
    available_zone = "us-east-1b"
    cidr = "172.31.2.0/24"
  }
}
nodes_config = {
  initial = 1
  max = 1
  min = 1
}

instance_types = ["t3.medium"]
```

| parameter        | type     | description                                    |
|------------------|----------|------------------------------------------------|
| `environment`    | string   | Environment                                    |
| `region`         | string   | AWS Region the cluster will be created in      |
| `cidr`           | string   | environment cidr                               |
| `subnets`        | object   | subnets separated by availability zones        |
| `nodes_config`   | object   | Cluster node configuration (initial, min, max) |
| `instance_types` | []string | List of instance types that will be the nodes  |

### nginx service settings
The `nginx` application settings are in the `modules.tf` file
Through the module it is possible to create other applications by modifying only the `name` `image` `replicas` parameters
```terraform
module "application" {
   source = "./modules/application"
   environment = var.environment
   cluster_name = module.cluster.cluster_name
   cluster_endpoint = module.cluster.cluster_endpoint
   cluster_ca_cert = module.cluster.cluster_ca_cert
   name = "nginx"
   image = "nginx"
   replicas = 2
}
```

## Creating cluster & application

In the repository, there is a `script` to facilitate the use of terraform.
The `script` automatically inserts the necessary parameters to use apply environment settings

```shell
./bin/tf.sh <ENVIRONMENT> <TERRAFORM_COMMAND> <TERRAFORM_ARGS>
```

### Starting

Run command to start cluster settings for a given
environment (environments must be in config/environments/<ENVIRONMENT>.tfvars).
In the examples we will create the `dev` environment

```shell
./bin/tf.sh dev init
```

After running `init` we will check the cluster creation plan, with the `plan` command, this step is not
mandatory, but we have a direct view of what will be created/modified without affecting our cluster

```shell
./bin/tf.sh dev plan
```

Now let's run the component creation (**This process may take a while**)

```shell
./bin/tf.sh dev apply
```

An option will appear to confirm the creation, just type `yes` to continue

When the process is finished, we have our service running on AWS.

### Viewing service

To view the service, we have three options for acquiring the service address:

- Kubernetes commands
- aws-cli commands
- View using aws console

#### Using kubernetes

```shell
aws eks update-kubeconfig --name app-<ENVIRONMENT>-cluster # to configure the cluster in your kubeconfig
kubectl get ingress # Result will bring the Load Balancer endpoint to access
```

#### Using aws-cli

```shell
aws elb describe-load-balancers | grep "CanonicalHostedZoneName:"
```

Use this endpoint to access the service (If there is more than one LoadBalancer, use the option above)

#### Using aws console
Go to: https://us-east-1.console.aws.amazon.com/ec2/home?region=us-east-1 (The address is different based on the region of your environment)
or search the AWS console for `Load Balancers` in the `EC2` service


## Why Ingress Nginx?

In this code we are using `ingress nginx` (https://github.com/kubernetes/ingress-nginx) to make the k8s service available to the internet.
This is because we can configure subdomains to access services, such as:
`page.meunginx.com` or `web.meunginx.com`, instead of using multiple Load Balancers for each created service
(Which would be much more expensive than using NGINX)

## Deploy with Gitlab CI/CD

To perform the deployment, it is necessary to configure the environment variables.
To do so, access `Settings` > `CI/CD` > `Variables` and register the following variables with the values of a
user with aws permission.

- AWS_ACCESS_KEY_ID
- AWS_SECRET_ACCESS_KEY

Also configure a `TF_BACKEND_FILE` variable of type `file` with the following content, modifying only the destination bucket

```terraform
terraform {
  backend "s3" {
    bucket = "" # Configure your bucket
    region = "us-east-1"
  }
}
```

The deployment of the `dev` environment will occur on every merge or push to `main`.

The deployment of the `prod` and `staging` environment will occur whenever a new commit tag is created