#!/usr/bin/env bash
pathScript=$(dirname "$0")
TERRAFORM_CMD="$2"
TERRAFORM_ARGS="${@:3}"
ENVIRONMENT="$1"
CONFIG_ENVIRONMENT="config/environments/$ENVIRONMENT.tfvars"
source "$pathScript/helpers.sh"

help_cmds(){
  print_info "use: "
  print_info "    ./bin/tf.sh <plan|apply|init|destroy> <ENVIRONMENT>"
  print_info "ex:"
  print_info "   ./bin/tf.sh apply dev"
}

if [ $# -lt 2 ]
then
  help_cmds
  exit 0
fi


if [ ! -f "$CONFIG_ENVIRONMENT" ]
then
  print_error "File $CONFIG_ENVIRONMENT not found"
  exit 1
fi



tfinit() {
  terraform  init -backend-config="key=${ENVIRONMENT}/terraform.tfstate" --reconfigure
}

tfapply() {
  tfinit
  terraform apply -var-file "${CONFIG_ENVIRONMENT}" $TERRAFORM_ARGS
}

tfplan() {
  tfinit
  terraform plan -var-file "${CONFIG_ENVIRONMENT}" $TERRAFORM_ARGS
}


tfdestroy() {
  tfinit
  terraform destroy -var-file  "${CONFIG_ENVIRONMENT}" $TERRAFORM_ARGS
}

case "$TERRAFORM_CMD" in
  "plan")
    tfplan;;
  "apply")
    tfapply;;
  "destroy")
    tfdestroy;;
  "init")
    tfinit;;
  *) terraform $TERRAFORM_CMD $TERRAFORM_ARGS
esac



