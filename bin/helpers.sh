#!/usr/bin/env bash

print_error() {
  echo -e "\033[1;31m$1\033[0m"
}

print_success() {
  echo -e "\033[1;32m$1\033[0m"
}

print_warn() {
  echo -e "\033[1;33m$1\033[0m"
}

print_info() {
  echo -e "\033[1;34m$1\033[0m"
}