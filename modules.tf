



module "cluster" {
  source         = "./modules/cluster"
  environment    = var.environment
  cidr           = var.cidr
  subnets        = var.subnets
  instance_types = var.instance_types
  nodes_config   = var.nodes_config
}

module "addons" {
  source = "./modules/addons"
  environment = var.environment
  cluster_name = module.cluster.cluster_name
  cluster_endpoint = module.cluster.cluster_endpoint
  cluster_ca_cert = module.cluster.cluster_ca_cert
}

module "application" {
  source = "./modules/application"
  environment = var.environment
  cluster_name = module.cluster.cluster_name
  cluster_endpoint = module.cluster.cluster_endpoint
  cluster_ca_cert = module.cluster.cluster_ca_cert
  name = "nginx"
  image = "nginx"
  replicas = 2
}