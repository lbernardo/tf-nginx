# tf-nginx

- [EN](readme-en.md)

## Dependências

- aws-cli [~2.7]
- terraform [v1.2.9]
- kubectl [1.23]

## Configurações iniciais

É necesário realizar as configurações de backend do terraform através do arquivo `backend.tf`.
Na configuração backend.bucket, adicione o bucket s3 que deseja utilizar para salvar o arquivo de estado do terraform.
É necessário que esse bucket exista, e que o `profile` default tenha permissão de acesso.
Caso necessário você pode configurar a região (`region`)  no parâmetro backend.region

```terraform
terraform {
  backend "s3" {
    bucket = "exemplo-de-bucket"
    region = "us-east-1"
  }
}
```

Também será necessário configurar as credenciais AWS no seu terminal

```shell
export AWS_ACCESS_KEY_ID=******
export AWS_SECRET_ACCESS_KEY=*******
```

Ou configurar um `profile` nos arquivos:

- `backend.tf`
- `modules/addons/providers.tf`
- `modules/application/providers.tf`
- `main.tf`

### Configurando ambiente
Existem configurações pré-definidas de ambiente (environment). Elas ficam em `config/environments/<ENVIRONMENT>.tfvars`.
Nesse arquivo você encontrará informações de configuração do cluster por ambiente.

```terraform
environment = "dev"
region      = "us-east-1"
cidr        = "172.31.0.0/16"
subnets     = {
  a = {
    avaiable_zone = "us-east-1a"
    cidr          = "172.31.1.0/24"
  }
  b = {
    avaiable_zone = "us-east-1b"
    cidr          = "172.31.2.0/24"
  }
}
nodes_config = {
  initial = 1
  max     = 1
  min     = 1
}

instance_types = ["t3.medium"]
```

| parâmetro        | tipo     | descrição                                          |
|------------------|----------|----------------------------------------------------|
| `environment`    | string   | Ambiente                                           |
| `region`         | string   | Região da AWS que o cluster irá ser criado         |
| `cidr`           | string   | cidr do ambiente                                   |
| `subnets`        | object   | subnetes separadas por zonas de disponibilidade    |
| `nodes_config`   | object   | Configuração de nós do cluster (initial, min, max) |
| `instance_types` | []string | Lista de tipos de instancias que serão os nós      |


### Configurações do serviço nginx
As configurações da aplicação `nginx` fica no arquivo`modules.tf`
Através do módulo é possível criar outras aplicações modificando somente os parâmetros `name` `image` `replicas`
```terraform
module "application" {
  source = "./modules/application"
  environment = var.environment
  cluster_name = module.cluster.cluster_name
  cluster_endpoint = module.cluster.cluster_endpoint
  cluster_ca_cert = module.cluster.cluster_ca_cert
  name = "nginx"
  image = "nginx"
  replicas = 2
}
```

## Criando cluster & aplicação

No repositório, existe um `script` para facilitar a utilização do terraform.
O `script` automaticamente insere os parâmetros necessário para utilizar aplicar as configurações do ambiente

```shell
./bin/tf.sh <ENVIRONMENT> <TERRAFORM_COMMAND> <TERRAFORM_ARGS>
```

### Iniciando

Execute o comando para iniciar as configurações do cluster de um determinado
ambiente (os ambientes devem estar em config/environments/<ENVIRONMENT>.tfvars).
Nos exemplos vamos criar o ambiente `dev`

```shell
./bin/tf.sh dev init
```

Após executar o `init` iramos verificar o plano de criação do cluster, com o comando `plan`, este passo não é 
obrigatório, mas temos uma visão direta do que será criado/modificado sem afetar nosso cluster

```shell
./bin/tf.sh dev plan
```

Agora vamos executar a criação dos componentes (**Esse processo pode demorar um pouco**)

```shell
./bin/tf.sh dev apply
```

Irá aparecer uma opção para confirmar a criação, basta digitar `yes` para continuar

Quando processo terminar, temos nosso serviço rodando na AWS.

### Visualizando serviço

Para acesar o serviço basta acessar o output de retorno do terraform depois de executar o apply.

ex:
```shell
host_app = "http://xxxxx"
```

Você também pode executar o seguinte comando para visualizar o output

```shell
terraform output
```

## Porque Ingress Nginx?

Nesse código estamos utilizando `ingress nginx` (https://github.com/kubernetes/ingress-nginx) para realizar a disponibilização do serviço do k8s para internet.
Isso porque podemos configurar subdominios para acessar os serviços, como por exemplo: 
`page.meunginx.com` ou `web.meunginx.com`, ao invés de utilizar vários Load Balancers para cada serviço criado
(O que sairia bem mais caro do que a utilização do NGINX)

## Deploy com GItlab CI/CD

Para realizar o deploy, é necessário realizar as configurações de variáveis de ambiente.
Para isso acesse `Settings` > `CI/CD` > `Variables` e cadastre as seguintes variáveis com os valores de um
usuário com permissão na aws. 

- AWS_ACCESS_KEY_ID
- AWS_SECRET_ACCESS_KEY

Configure também uma variavel `TF_BACKEND_FILE` do tipo `file` com o seguinte conteudo, modificando somente o bucket de destino

```terraform
terraform {
  backend "s3" {
    bucket = "" # Configure your bucket
    region = "us-east-1"
  }
}
```

O deploy do ambiente `dev` irá ocorrer em todos os merges ou push para `main`.

O deploy do ambiente `prod` e `staging` irá ocorrer sempre que for criada uma nova tag de commit