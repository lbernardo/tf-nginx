environment = "staging"
region      = "us-east-1"
cidr        = "172.33.0.0/16"
subnets     = {
  a = {
    avaiable_zone = "us-east-1a"
    cidr          = "172.33.1.0/24"
  }
  b = {
    avaiable_zone = "us-east-1b"
    cidr          = "172.33.2.0/24"
  }
}
nodes_config = {
  initial = 2
  max     = 2
  min     = 1
}

instance_types = ["t3.medium"]